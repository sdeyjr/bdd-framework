Feature: user being able to login

  @Regression
  Scenario: user can go to the cart page without loggin in

    Given a user open the chrome and navigate to https://www.amazon.com
    When a user can see the amazon homepage
    Then  user clicks on the cart button
    Then user validates cart page is displayed
    And user close the browser