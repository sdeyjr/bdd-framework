package pages;

import core.DriverSetUp;
import org.testng.Assert;

public class CartPage {

    public void validateCartPageLoaded () {
        String currentUrl = DriverSetUp.driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, "https://www.amazon.com/gp/cart/view.html?ref_=nav_cart");
    }

}
