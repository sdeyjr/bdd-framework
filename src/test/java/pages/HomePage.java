package pages;

import core.DriverSetUp;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(xpath = "//span[@class='nav-cart-icon nav-sprite']")
    private WebElement cartButton;

    public void validateHomePageLoaded() {
        String currentUrl = DriverSetUp.driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, "https://www.amazon.com/");
    }

    public void validateUserCanClickOnCartButton() {
        cartButton.click();
    }
}
