package stepdefinations;

import core.DriverSetUp;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pages.CartPage;
import pages.HomePage;

public class StepDef extends DriverSetUp {
    @Given("^a user open the chrome and navigate to https://www\\.amazon\\.com$")
    public void a_user_open_the_chrome_and_navigate_to_https_www_amazon_com() {
        initializeDriver();
        driver.get("https://www.amazon.com");
    }

    @When("^a user can see the amazon homepage$")
    public void a_user_can_see_the_amazon_homepage(){
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.validateHomePageLoaded();
    }

    @Then("^user clicks on the cart button$")
    public void user_clicks_on_the_cart_button() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.validateUserCanClickOnCartButton();
    }

    @Then("^user validates cart page is displayed$")
    public void user_validates_cart_page_is_displayed() {
        CartPage cartPage = PageFactory.initElements(driver,CartPage.class);
        cartPage.validateCartPageLoaded();
    }

    @And("^user close the browser$")
    public void user_close_the_browser() {
        CartPage cartPage = PageFactory.initElements(driver,CartPage.class);
        driver.quit();
    }
}

